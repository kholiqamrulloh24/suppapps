import React, { useState, useEffect } from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HalamanUtama from "../screens/halamautama/HalamanUtama";
import Login from "../screens/Login";
import Register from "../screens/Register";
import { AppRouteParams } from "./type";
import storage from '@react-native-async-storage/async-storage';
import Verification from 'screens/Verification';
import Goods from 'screens/Goods';
import OutgoingNew from 'screens/OutgoingNew';
import Customer from 'screens/Customer';
import Document from 'screens/Document';

const Stack = createNativeStackNavigator<AppRouteParams>();
const Navigation = () => {
     const [initialRouteName, setInitialRouteName] = useState<keyof AppRouteParams>('Login')
     const [loading, setLoading] = useState(true);

     useEffect(() => {
          storage.getItem('token')
               .then((value: string | null) => {
                    if (value) {
                         setInitialRouteName('HalamanUtama');
                    }
                    setLoading(false);
               })
               .catch(() => {
                    setLoading(false);
               })
     }, [])

     if (loading) {
          return null;
     }


     return (
          <NavigationContainer>
               <Stack.Navigator initialRouteName={initialRouteName} screenOptions={{ headerShown: false }}>
                    <Stack.Screen name='Login' component={Login} />
                    <Stack.Screen name='Register' component={Register} />
                    <Stack.Screen name='HalamanUtama' component={HalamanUtama} />
                    <Stack.Screen name='Verification' component={Verification} />
                    <Stack.Screen name='Goods' component={Goods} />
                    <Stack.Screen name='OutgoingNew' component={OutgoingNew} />
                    <Stack.Screen name='Customer' component={Customer} />
                    <Stack.Screen name='Document' component={Document} />
               </Stack.Navigator>
          </NavigationContainer>
     );
}

export default Navigation;