import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import { AppRouteParams } from 'navigation/type';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Modal from 'react-native-modal';
import Logo from 'assets/images/Logo.svg';

type RegisterRouteParams = NativeStackScreenProps<AppRouteParams, 'Register'>

const Register = ({ navigation }: RegisterRouteParams) => {
     const [username, setUsername] = useState('');
     const [password, setPassword] = useState('');
     const [modal, setModal] = useState(false);

     const togleModal = () => {
          setModal(!modal);
     }

     const Register = () => {
          setModal(!modal);
          setUsername('');
          setPassword('');
     }
     return (
          <>
               <View style={{ flex: 1, marginHorizontal: 30 }}>
                    <View style={{ marginTop: 100, alignItems: 'center', justifyContent: 'center' }}>
                         <Logo width={150} height={150} />
                         <Text style={{ fontSize: 50, fontWeight: 'bold' }}>Sign Up</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ backgroundColor: '#D9D9D9', height: 40, paddingHorizontal: 25, borderRadius: 25 }} placeholder='first name' />
                         </View>
                         <View style={{ marginHorizontal: 10 }}></View>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ backgroundColor: '#D9D9D9', height: 40, paddingHorizontal: 25, borderRadius: 25 }} placeholder='last name' />
                         </View>
                    </View>
                    <View style={{ marginTop: 10 }}>
                         <TextInput style={{ backgroundColor: '#D9D9D9', height: 40, borderRadius: 25, paddingHorizontal: 25 }} placeholder='email' />
                    </View>
                    <View style={{ marginTop: 10 }}>
                         <TextInput style={{ backgroundColor: '#D9D9D9', height: 40, borderRadius: 25, paddingHorizontal: 25 }} placeholder='password' />
                    </View>
                    <View style={{ marginTop: 10 }}>
                         <TextInput style={{ backgroundColor: '#D9D9D9', height: 40, borderRadius: 25, paddingHorizontal: 25 }} placeholder='phone number' />
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                         <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Verification')}>
                              <Text style={{ color: 'white' }}>Continue</Text>
                         </TouchableOpacity>
                         <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center' }}>
                              <Text>Already have an account</Text>
                              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                   <Text style={{ color: 'blue', textDecorationLine: 'underline' }}>Sign in!</Text>
                              </TouchableOpacity>
                         </View>
                    </View>
               </View>
               <Modal isVisible={modal} style={{ justifyContent: 'center', alignItems: 'center' }} onBackdropPress={togleModal}>
                    <View style={styles.modal}>
                         <Text>Berhasil Register</Text>
                    </View>
               </Modal>
          </>
     );
}

const styles = StyleSheet.create({
     items: {
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50
     },
     button: {
          backgroundColor: '#23AA49',
          marginTop: 10,
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
          borderRadius: 20,
          width: 200
     },
     modal: {
          width: 320,
          height: 130,
          backgroundColor: '#445E93',
          borderRadius: 25,
          justifyContent: 'center',
          alignItems: 'center'
     }
})

export default Register;