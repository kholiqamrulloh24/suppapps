import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet, ActivityIndicator, ScrollView } from 'react-native'
import { AppRouteParams } from 'navigation/type';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Modal from 'react-native-modal';
import axios from 'apis/app-axios';
import { LoginResponse } from 'types/api-responses/login-response';
import Toast from 'react-native-toast-message';
import * as apiEndPoints from 'constants/api-endpoints';
import { Formik } from 'formik';
import * as yup from 'yup';
import storage from '@react-native-async-storage/async-storage';
import Logo from 'assets/images/Logo.svg';

type LoginRouteParams = NativeStackScreenProps<AppRouteParams, 'Login'>

interface FormLogin {
     username: string,
     password: string
}

const Login = ({ navigation }: LoginRouteParams) => {
     const [loading, setLoading] = useState(false);
     const [modal, setModal] = useState(false);
     const togleModal = () => {
          setModal(!modal);
     }

     const doLogin = (values: FormLogin) => {
          setLoading(true);
          axios.post<LoginResponse>(apiEndPoints.LOGIN, {
               username: values.username,
               password: values.password
          }).then(res => {
               let target: keyof AppRouteParams;
               switch (res.data) {
                    default:
                         target = "HalamanUtama";
                         break;
               }
               storage.setItem('token', res.data.token)
                    .then(() => { })
               navigation.reset({
                    routes: [
                         { name: target }
                    ],
                    index: 0
               })
               setLoading(false);
          }).catch(e => {
               Toast.show({
                    type: 'error',
                    text1: 'Warning !',
                    text2: 'Username atau password salah!'
               })
               setLoading(false)
          })
     }

     return (
          <>
               <ScrollView style={{ flex: 1, marginHorizontal: 30 }}>
                    <View style={{ marginTop: 100, alignItems: 'center', justifyContent: 'center' }}>
                         <Logo width={150} height={150} />
                         <Text style={{ fontSize: 50, fontWeight: 'bold' }}>Login</Text>
                    </View>
                    <Toast />
                    <View style={styles.items}>
                         <Formik
                              validationSchema={loginValidationSchema}
                              initialValues={{ username: '', password: '' }}
                              onSubmit={doLogin}
                         >
                              {({ setFieldValue, handleSubmit, values, errors, isValid }) => (
                                   <>
                                        <View style={{ flexDirection: 'row' }}>
                                             <TextInput
                                                  style={{ flex: 1, paddingLeft: 20, height: 60, backgroundColor: '#D9D9D9', borderRadius: 25 }}
                                                  placeholder='email'
                                                  onChangeText={(username) => setFieldValue('username', username)}
                                                  value={values.username}
                                                  keyboardType="email-address"
                                             />
                                        </View>
                                        {errors.username &&
                                             <Text style={{ fontSize: 10, color: 'red' }}>{errors.username}</Text>
                                        }
                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                             <TextInput
                                                  style={{ flex: 1, paddingLeft: 20, height: 60, backgroundColor: '#D9D9D9', borderRadius: 25 }}
                                                  placeholder='password'
                                                  secureTextEntry={true}
                                                  value={values.password}
                                                  onChangeText={(password) => setFieldValue('password', password)}
                                             />
                                        </View>
                                        {errors.password &&
                                             <Text style={{ fontSize: 10, color: 'red' }}>{errors.password}</Text>
                                        }
                                        <View style={{ flexDirection: 'row' }}>
                                             <TouchableOpacity style={styles.button} onPress={handleSubmit} disabled={!isValid}>
                                                  {loading &&
                                                       <ActivityIndicator color="red" />
                                                  }
                                                  {!loading &&
                                                       <Text style={{ color: 'white' }}>Sign in</Text>
                                                  }
                                             </TouchableOpacity>
                                        </View>
                                   </>
                              )}
                         </Formik>
                         <View style={{ marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                              <Text>If you don't have an account, please</Text>
                              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                                   <Text style={{ color: 'blue', textDecorationLine: 'underline' }}>sign up here!</Text>
                              </TouchableOpacity>
                         </View>
                    </View>
               </ScrollView>
               <Modal isVisible={modal} style={{ justifyContent: 'center', alignItems: 'center' }} onBackdropPress={togleModal} onBackButtonPress={togleModal}>
                    <View style={styles.modal}>
                         <Text>Data Berhasil Dikirim</Text>
                    </View>
               </Modal>
          </>
     );
}

const styles = StyleSheet.create({
     items: {
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50
     },
     button: {
          backgroundColor: '#23AA49',
          marginTop: 10,
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
          borderRadius: 20,
          width: 200
     },
     modal: {
          width: 320,
          height: 130,
          backgroundColor: '#445E93',
          borderRadius: 25,
          justifyContent: 'center',
          alignItems: 'center'
     }
})

const loginValidationSchema = yup.object().shape({
     username: yup
          .string()
          .email("Please enter valid email")
          .required('Email Address is Required'),
     password: yup
          .string()
          .min(8, ({ min }) => `Password must be at least ${min} characters`)
          .required('Password is required'),
})

export default Login;