import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import { AppRouteParams } from 'navigation/type';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Modal from 'react-native-modal';
import Logo from 'assets/images/Logo.svg';

type VerificationRouteParams = NativeStackScreenProps<AppRouteParams, 'Verification'>

const Verification = ({ navigation }: VerificationRouteParams) => {
     const [username, setUsername] = useState('');
     const [password, setPassword] = useState('');
     const [modal, setModal] = useState(false);

     const togleModal = () => {
          setModal(!modal);
     }

     const Register = () => {
          setModal(!modal);
          setUsername('');
          setPassword('');
     }
     return (
          <>
               <View style={{ flex: 1, marginHorizontal: 30 }}>
                    <View style={{ marginTop: 100, alignItems: 'center', justifyContent: 'center' }}>
                         <Logo width={150} height={150} />
                    </View>
                    <View style={{ marginTop: 30, alignItems: 'center', justifyContent: 'center' }}>
                         <Text>Enter the 4 digit code send to</Text>
                         <Text style={{ fontWeight: 'bold' }}>+62 00000000000</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30 }}>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ borderWidth: 1, borderColor: '#23AA49', backgroundColor: '#D9D9D9', height: 80 }} />
                         </View>
                         <View style={{ marginHorizontal: 5 }}></View>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ borderWidth: 1, borderColor: '#23AA49', backgroundColor: '#D9D9D9', height: 80 }} />
                         </View>
                         <View style={{ marginHorizontal: 5 }}></View>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ borderWidth: 1, borderColor: '#23AA49', backgroundColor: '#D9D9D9', height: 80 }} />
                         </View>
                         <View style={{ marginHorizontal: 5 }}></View>
                         <View style={{ flex: 1 }}>
                              <TextInput style={{ borderWidth: 1, borderColor: '#23AA49', backgroundColor: '#D9D9D9', height: 80 }} />
                         </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                         <Text>Wrong phone number?</Text>
                         <TouchableOpacity>
                              <Text style={{ color: 'blue' }}>Update now</Text>
                         </TouchableOpacity>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                         <TouchableOpacity>
                              <Text style={{ color: 'blue', fontSize: 25 }}>Ask for new code</Text>
                         </TouchableOpacity>
                    </View>
               </View>
               <Modal isVisible={modal} style={{ justifyContent: 'center', alignItems: 'center' }} onBackdropPress={togleModal}>
                    <View style={styles.modal}>
                         <Text>Berhasil Register</Text>
                    </View>
               </Modal>
          </>
     );
}

const styles = StyleSheet.create({
     items: {
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50
     },
     button: {
          backgroundColor: '#23AA49',
          marginTop: 10,
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
          borderRadius: 20,
          width: 200
     },
     modal: {
          width: 320,
          height: 130,
          backgroundColor: '#445E93',
          borderRadius: 25,
          justifyContent: 'center',
          alignItems: 'center'
     }
})

export default Verification;