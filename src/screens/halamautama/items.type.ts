import { ReactNode } from "react";

export interface ItemsProps {
     image: ReactNode;
     title: string;
     onPress: () => void;
}