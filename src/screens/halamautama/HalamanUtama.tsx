import storage from '@react-native-async-storage/async-storage';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Customer from 'assets/images/customer.svg';
import Document from 'assets/images/document.svg';
import Goods from 'assets/images/goods.svg';
import Garis from 'assets/images/horizontal.svg';
import Info from 'assets/images/info.svg';
import NavMenu from 'assets/images/NavMenu.svg';
import OutgoingNew from 'assets/images/OutgoingNew.svg';
import Shop from 'assets/images/Shop.svg';
import { AppRouteParams } from 'navigation/type';
import React, { useEffect, useState } from 'react';
import { Button, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Items from './items.component';
import Modal from 'react-native-modal';
import Report from 'assets/images/Report.svg';
import Dompet from 'assets/images/dompet.svg';
import Notif from 'assets/images/notif.svg';

type HalamanUtamaRouteParams = NativeStackScreenProps<AppRouteParams, 'HalamanUtama'>

const HalamanUtama = ({ navigation }: HalamanUtamaRouteParams) => {
     const [time, setTime] = useState('');
     const [isModalLogout, setIsModalLogout] = useState(false);

     const toogleLogout = () => {
          setIsModalLogout(!isModalLogout);
     }
     // useEffect(() => {
     //      setInterval(() => {
     //           const hour = new Date().getHours();
     //           const min = new Date().getMinutes();
     //           const sec = new Date().getSeconds();

     //           setTime(hour + ':' + min + ':' + sec)
     //      }, 1000)
     // }, [])

     const doLogout = () => {
          storage.removeItem('token')
               .then(() => navigation.navigate('Login'))
     }

     return (
          <>
               <ScrollView style={{ flex: 1, backgroundColor: '#23AA49' }}>
                    <View style={{ marginHorizontal: 10, marginTop: 40 }}>
                         <View style={{ flexDirection: 'row' }}>
                              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                   <TouchableOpacity onPress={toogleLogout}>
                                        <NavMenu width={30} height={30} />
                                   </TouchableOpacity>
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', marginLeft: 8 }}>
                                   <Text style={{ color: 'white', fontSize: 25, }}>Supplier Apps</Text>
                              </View>
                              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                   <TouchableOpacity>
                                        <Info width={25} height={25} />
                                   </TouchableOpacity>
                              </View>
                         </View>
                         <View style={{ flexDirection: 'row', marginTop: 10 }}>
                              <View style={{ flex: 1, backgroundColor: '#9CD9AD', marginRight: 10, borderRadius: 15 }}>
                                   <TextInput style={{ height: 50, paddingLeft: 15 }} placeholder='Main Store' placeholderTextColor='white' />
                              </View>
                              <View>
                                   <TouchableOpacity>
                                        <Shop width={50} height={50} />
                                   </TouchableOpacity>
                              </View>
                         </View>
                         <View style={{ marginTop: 10, flexDirection: 'row', backgroundColor: '#9CD9AD', borderRadius: 15, height: 50 }}>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Text style={{ color: 'white', fontSize: 18 }}>Current Stock</Text>
                                   <Text style={{ color: 'white', fontSize: 15 }}>0.00</Text>
                              </View>
                              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                   <Garis />
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Text style={{ color: 'white', fontSize: 18 }}>Stock Value</Text>
                                   <Text style={{ color: 'white', fontSize: 15 }}>Rp0.00</Text>
                              </View>
                         </View>
                         <View style={{ marginTop: 10 }}>
                              <Text style={{ color: 'white', fontSize: 20 }}>Dashboard</Text>
                         </View>
                         <View style={{ flexDirection: 'row', marginTop: 10 }}>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Items image={<Goods width={60} height={60} />} title='Goods' onPress={() => navigation.navigate('Goods')} />
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Items image={<OutgoingNew width={60} height={60} />} title='Outgoing New' onPress={() => navigation.navigate('OutgoingNew')} />
                              </View>
                         </View>
                         <View style={{ flexDirection: 'row', marginTop: 10 }}>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Items image={<Customer width={60} height={60} />} title='Customer' onPress={() => navigation.navigate('Customer')} />
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                   <Items image={<Document width={60} height={60} />} title='Document' onPress={() => navigation.navigate('Document')} />
                              </View>
                         </View>
                         {/* <TouchableOpacity>
                              <Text>Test</Text>
                         </TouchableOpacity>
                         <Text>{time}</Text> */}
                         <View style={{ marginTop: 10 }}>
                              <Text style={{ color: 'white', fontSize: 20 }}>Reports and Expenses</Text>
                         </View>
                         <View style={{ marginTop: 10, flexDirection: 'row', backgroundColor: '#9CD9AD', borderRadius: 15, height: 50 }}>
                              <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 20 }}>
                                   <Report width={40} height={40} />
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
                                   <Text style={{ color: 'white', fontSize: 18 }}>Inventory Reports</Text>
                              </View>
                         </View>
                         <View style={{ marginTop: 10, flexDirection: 'row', backgroundColor: '#9CD9AD', borderRadius: 15, height: 50 }}>
                              <View style={{ justifyContent: 'center', alignItems: 'center', marginHorizontal: 20 }}>
                                   <Dompet width={40} height={40} />
                              </View>
                              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
                                   <Text style={{ color: 'white', fontSize: 18 }}>Expenses</Text>
                                   <Text style={{ color: 'white', fontSize: 15 }}>Rp0.00</Text>
                              </View>
                         </View>
                         <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginTop: 15 }}>
                              <Notif width={80} height={80} />
                         </View>
                    </View>
               </ScrollView>
               <Modal isVisible={isModalLogout} style={{ justifyContent: 'center', alignItems: 'flex-start', marginHorizontal: 0, marginVertical: 0, flexDirection: 'column' }} onBackdropPress={toogleLogout} onBackButtonPress={toogleLogout} animationIn='slideInLeft' animationOut='slideOutLeft'>
                    <View style={styles.modal}>
                         <TouchableOpacity style={{ marginVertical: 10, backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', height: 40, width: 80, borderRadius: 10 }} onPress={doLogout}>
                              <Text>Logout</Text>
                         </TouchableOpacity>
                    </View>
               </Modal>
          </>
     );
}

const styles = StyleSheet.create({
     modal: {
          width: 150,
          height: 130,
          backgroundColor: '#9CD9AD',
          justifyContent: 'flex-end',
          alignItems: 'center',
          flex: 1
     }
})

export default HalamanUtama;