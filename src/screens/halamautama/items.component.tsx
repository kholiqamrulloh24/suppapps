import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { ItemsProps } from './items.type';

const Items = (props: ItemsProps) => {
     const { image, title, onPress } = props;
     return (
          <TouchableOpacity style={styles.box} onPress={onPress} >
               {image}
               <Text style={{ color: 'green' }}>{title}</Text>
          </TouchableOpacity>
     );
}

const styles = StyleSheet.create({
     box: {
          backgroundColor: '#FFFFFF',
          height: 100,
          width: 120,
          borderRadius: 15,
          justifyContent: 'center',
          alignItems: 'center'
     }
})

export default Items;