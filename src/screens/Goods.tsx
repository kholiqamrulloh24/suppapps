import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import Back from 'assets/images/Arrow.svg';
import Search from 'assets/images/Search.svg';
import Opsi from 'assets/images/titiktiga.svg';
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { AppRouteParams } from "navigation/type";
import Good from 'assets/images/goods.svg';
import Modal from 'react-native-modal';
import Import from 'assets/images/import.svg';

type GoodsRouteParams = NativeStackScreenProps<AppRouteParams, 'Goods'>

const Goods = ({ navigation }: GoodsRouteParams) => {
     const [modal, setModal] = useState(false);
     const toggleModal = () => {
          setModal(!modal);
     }
     return (
          <>
               <View style={{ flex: 1 }}>
                    <View style={{ marginTop: 60, marginHorizontal: 10 }}>
                         <View style={{ flexDirection: 'row' }}>
                              <View style={{ justifyContent: "center", alignItems: "center", marginRight: 10 }}>
                                   <TouchableOpacity onPress={() => navigation.goBack()}>
                                        <Back width={30} height={30} />
                                   </TouchableOpacity>
                              </View>
                              <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center", backgroundColor: '#D9D9D9', height: 40, borderRadius: 20 }}>
                                   <View style={{ marginHorizontal: 10 }}>
                                        <Search width={20} height={20} />
                                   </View>
                                   <View style={{ flex: 1 }}>
                                        <TextInput placeholder="Search category" />
                                   </View>
                              </View>
                              <View style={{ justifyContent: "center", alignItems: "center" }}>
                                   <TouchableOpacity onPress={toggleModal}>
                                        <Opsi width={30} height={30} />
                                   </TouchableOpacity>
                              </View>
                         </View>
                         <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 130 }}>
                              <Good width={200} height={200} />
                              <Text style={{ color: 'green', fontSize: 20 }}>No Data</Text>
                              <Text style={{ color: 'green' }}>Add your goods</Text>
                         </View>
                    </View>
               </View>
               <Modal isVisible={modal} style={{ justifyContent: 'flex-start', alignItems: 'flex-end', marginRight: 30, marginTop: 70 }} onBackdropPress={toggleModal} animationIn='fadeIn' animationOut='fadeOut' onBackButtonPress={toggleModal}>
                    <TouchableOpacity style={styles.modal}>
                         <View>
                              <Import width={20} height={20} />
                         </View>
                         <View style={{ marginHorizontal: 5 }}></View>
                         <View>
                              <Text style={{ color: 'white', fontSize: 15 }}>Import Data</Text>
                         </View>
                    </TouchableOpacity>
               </Modal>
          </>
     );
}

const styles = StyleSheet.create({
     modal: {
          width: 200,
          height: 40,
          backgroundColor: 'green',
          alignItems: 'center',
          borderRadius: 10,
          flexDirection: 'row',
          paddingHorizontal: 10
     }
})

export default Goods;