import AsyncStorage from "@react-native-async-storage/async-storage";
import defaultAxios, { AxiosHeaders } from "axios";
import * as apiEndPoints from 'constants/api-endpoints';

const axios = defaultAxios.create({
     baseURL: apiEndPoints.BASE_URL,
     timeout: 15000
});

axios.interceptors.request.use(async (config) => {
     let accessToken = await AsyncStorage.getItem('accessToken');

     if (accessToken) {
          config.headers = {
               ...config.headers,
               Authorization: `Bearer ${accessToken}`
          }
     }

     return config;
});

export default axios;
