import { StatusBar } from 'expo-status-bar';
import Navigation from 'navigation';
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App() {
  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor='#23AA49' />
      <Navigation />
    </SafeAreaProvider>
  );
}
